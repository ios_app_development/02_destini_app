# 02_Destini_app

## This app are “choose your own adventure” game similar to the App Store hit “Life Line” app.


* The app will tell a story depending on what the user chooses and can be fleshed out and modified to provide an engaging story-telling experience

## Visual Presentation

### Preview
![Static image](documentation/screen_1.png)
![Gif example](https://media.giphy.com/media/idM9402TSFoCNgwh3m/giphy.gif)


